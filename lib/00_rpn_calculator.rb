class RPNCalculator

  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def value
    @stack.last
  end

  def plus
    calculations(:+)
  end

  def minus
    calculations(:-)
  end

  def divide
    calculations(:/)
  end

  def times
    calculations(:*)
  end

  def tokens(input)
    input.split.map{|input| input =~ /\d/ ? input.to_i : input.to_sym}
  end

  def evaluate(string)
    token = tokens(string)
    token.each do |el|
      case el
      when Integer
        @stack << el
      else
        calculations(el)
      end
    end
      value
  end


  private

  def calculations(user_input)
    raise "calculator is empty" if @stack.size < 1
    variables = @stack.pop(2).map(&:to_f)

    case user_input
    when (:+)
      @stack << variables.inject(&:+)
    when (:-)
      @stack << variables.first - variables.last
    when (:*)
      @stack << variables.inject(&:*)
    when (:/)
      @stack << variables.first / variables.last
    else
      @stack << variables
      raise "#{user_input} is not a valid operation"
    end
  end
end
